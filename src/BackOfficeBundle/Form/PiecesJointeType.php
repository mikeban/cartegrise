<?php

namespace BackOfficeBundle\Form;

use BackOfficeBundle\Entity\PiecesJointe;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class PiecesJointeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nomDoc')
                ->add('etatDoc', CollectionType::class, [
                    'entry_type'   => ChoiceType::class,
                    'entry_options'  => [
                        'choices'  => [
                            'en attente' => 'en atente',
                            'bon'     => 'bon']]])
                ->add('idDemande')
                ->add('brochure', FileType::class, array('data_class' => null, 'required' => false), ['label' => 'Brochure (PDF file)']);
    } 
     /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BackOfficeBundle\Entity\PiecesJointe'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'backofficebundle_piecesjointe';
    }


}
