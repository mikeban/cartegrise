<?php

namespace BackOfficeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class DemandeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $dateNow= new \DateTime('now');
        $builder->add('immatriculation')
                ->add('statusDemande', CollectionType::class, [
                    'entry_type'   => ChoiceType::class,
                    'entry_options'  => [
                'choices'  => [
                    'en attente' => 'en atente',
                    'bon'     => 'bon'

                ]
            ]])
                ->add('besoinInfo')
                ->add('dateDelai',  DateType::class, [
                    // renders it as a single text box
                    'widget' => 'single_text'])
                ->add('infoDelais')
                ->add('departement')
                ->add('dateDemande', DateTimeType::class, [
                    // renders it as a single text box
                    'widget' => 'single_text','data' => $dateNow]);    
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BackOfficeBundle\Entity\Demande'
        ));
    }


    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'backofficebundle_demande';
    }


}
