<?php

namespace BackOfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tarif
 *
 * @ORM\Table(name="tarif")
 * @ORM\Entity
 */
class Tarif
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_tarif", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idTarif;

    /**
     * @var string
     *
     * @ORM\Column(name="type_tarif", type="string", length=50, nullable=false)
     */
    private $typeTarif;

    /**
     * @var int
     *
     * @ORM\Column(name="valeur_tarif", type="integer", nullable=false)
     */
    private $valeurTarif;



    /**
     * Get idTarif.
     *
     * @return int
     */
    public function getIdTarif()
    {
        return $this->idTarif;
    }

    /**
     * Set typeTarif.
     *
     * @param string $typeTarif
     *
     * @return Tarif
     */
    public function setTypeTarif($typeTarif)
    {
        $this->typeTarif = $typeTarif;

        return $this;
    }

    /**
     * Get typeTarif.
     *
     * @return string
     */
    public function getTypeTarif()
    {
        return $this->typeTarif;
    }

    /**
     * Set valeurTarif.
     *
     * @param int $valeurTarif
     *
     * @return Tarif
     */
    public function setValeurTarif($valeurTarif)
    {
        $this->valeurTarif = $valeurTarif;

        return $this;
    }

    /**
     * Get valeurTarif.
     *
     * @return int
     */
    public function getValeurTarif()
    {
        return $this->valeurTarif;
    }
}
