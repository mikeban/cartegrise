<?php

namespace BackOfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * PiecesJointe
 *
 * @ORM\Table(name="pieces_jointe", indexes={@ORM\Index(name="id_demande", columns={"id_demande"})})
 * @ORM\Entity
 */
class PiecesJointe
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_doc", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idDoc;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_doc", type="string", length=256, nullable=false)
     */
    private $nomDoc;

    /**
     * @var array
     *
     * @ORM\Column(name="etat_doc", type="simple_array", length=0, nullable=false, options={"default"="en traitement"})
     */
    private $etatDoc=["en attente"];

    /**
     * @var int
     *
     * @ORM\Column(name="id_demande", type="integer", nullable=false)
     */
    private $idDemande;

    /**
     * @var string
     *
     * @ORM\Column(name="brochure", type="string", length=128, nullable=false)
     */
    private $brochure;



    /**
     * Get idDoc.
     *
     * @return int
     */
    public function getIdDoc()
    {
        return $this->idDoc;
    }

    /**
     * Set nomDoc.
     *
     * @param string $nomDoc
     *
     * @return PiecesJointe
     */
    public function setNomDoc($nomDoc)
    {
        $this->nomDoc = $nomDoc;

        return $this;
    }

    /**
     * Get nomDoc.
     *
     * @return string
     */
    public function getNomDoc()
    {
        return $this->nomDoc;
    }

    /**
     * Set etatDoc.
     *
     * @param array $etatDoc
     *
     * @return PiecesJointe
     */
    public function setEtatDoc($etatDoc)
    {
        $this->etatDoc = $etatDoc;

        return $this;
    }

    /**
     * Get etatDoc.
     *
     * @return array
     */
    public function getEtatDoc()
    {
        return $this->etatDoc;
    }

    /**
     * Set idDemande.
     *
     * @param int $idDemande
     *
     * @return PiecesJointe
     */
    public function setIdDemande($idDemande)
    {
        $this->idDemande = $idDemande;

        return $this;
    }

    /**
     * Get idDemande.
     *
     * @return int
     */
    public function getIdDemande()
    {
        return $this->idDemande;
    }

    /**
     * Set brochure.
     *
     * @param string $brochure
     *
     * @return PiecesJointe
     */
    public function setBrochure($brochure)
    {
        $this->brochure = $brochure;

        return $this;
    }

    /**
     * Get brochure.
     *
     * @return string
     */
    public function getBrochure()
    {
        return $this->brochure;
    }
}
