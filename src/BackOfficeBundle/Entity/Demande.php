<?php

namespace BackOfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Demande
 *
 * @ORM\Table(name="demande")
 * @ORM\Entity
 */
class Demande
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_demande", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idDemande;

    /**
     * @var int
     *
     * @ORM\Column(name="immatriculation", type="integer", nullable=false)
     */
    private $immatriculation;

    /**
     * @var array
     *
     * @ORM\Column(name="status_demande", type="simple_array", length=0, nullable=false)
     */
    private $statusDemande=["en attente"];

    /**
     * @var bool
     *
     * @ORM\Column(name="besoin_info", type="boolean", nullable=false)
     */
    private $besoinInfo;

    /**
     * @var \Date
     *
     * @ORM\Column(name="date_delai", type="date", nullable=false)
     */
    private $dateDelai;

    /**
     * @var string
     *
     * @ORM\Column(name="info_delais", type="string", length=128, nullable=false)
     */
    private $infoDelais;

    /**
     * @var int
     *
     * @ORM\Column(name="departement", type="integer", nullable=false)
     */
    private $departement;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_demande", type="datetime", nullable=true)
     */
    private $dateDemande;


    /**
     * Get idDemande.
     *
     * @return int
     */
    public function getIdDemande()
    {
        return $this->idDemande;
    }

    /**
     * Set immatriculation.
     *
     * @param int $immatriculation
     *
     * @return Demande
     */
    public function setImmatriculation($immatriculation)
    {
        $this->immatriculation = $immatriculation;

        return $this;
    }

    /**
     * Get immatriculation.
     *
     * @return int
     */
    public function getImmatriculation()
    {
        return $this->immatriculation;
    }

    /**
     * Set statusDemande.
     *
     * @param array $statusDemande
     *
     * @return Demande
     */
    public function setStatusDemande($statusDemande)
    {
        $this->statusDemande = $statusDemande;

        return $this;
    }

    /**
     * Get statusDemande.
     *
     * @return array
     */
    public function getStatusDemande()
    {
        return $this->statusDemande;
    }

    /**
     * Set besoinInfo.
     *
     * @param bool $besoinInfo
     *
     * @return Demande
     */
    public function setBesoinInfo($besoinInfo)
    {
        $this->besoinInfo = $besoinInfo;

        return $this;
    }

    /**
     * Get besoinInfo.
     *
     * @return bool
     */
    public function getBesoinInfo()
    {
        return $this->besoinInfo;
    }

    /**
     * Set dateDelai.
     *
     * @param \DateTime $dateDelai
     *
     * @return Demande
     */
    public function setDateDelai($dateDelai)
    {
        $this->dateDelai = $dateDelai;

        return $this;
    }

    /**
     * Get dateDelai.
     *
     * @return \DateTime
     */
    public function getDateDelai()
    {
        return $this->dateDelai;
    }

    /**
     * Set infoDelais.
     *
     * @param string $infoDelais
     *
     * @return Demande
     */
    public function setInfoDelais($infoDelais)
    {
        $this->infoDelais = $infoDelais;

        return $this;
    }

    /**
     * Get infoDelais.
     *
     * @return string
     */
    public function getInfoDelais()
    {
        return $this->infoDelais;
    }

    /**
     * Set departement.
     *
     * @param int $departement
     *
     * @return Demande
     */
    public function setDepartement($departement)
    {
        $this->departement = $departement;

        return $this;
    }

    /**
     * Get departement.
     *
     * @return int
     */
    public function getDepartement()
    {
        return $this->departement;
    }

    /**
     * Set dateDemande.
     *
     * @param \DateTime $dateDemande
     *
     * @return Demande
     */
    public function setDateDemande($dateDemande)
    {
        $this->dateDemande = $dateDemande;

        return $this;
    }

    /**
     * Get dateDemande.
     *
     * @return \DateTime
     */
    public function getDateDemande()
    {
        return $this->dateDemande;
    }
}
