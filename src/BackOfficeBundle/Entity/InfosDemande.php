<?php

namespace BackOfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfosDemande
 *
 * @ORM\Table(name="infos_demande", indexes={@ORM\Index(name="id_demande", columns={"id_demande"})})
 * @ORM\Entity
 */
class InfosDemande
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_info", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="texte_info_demande", type="string", length=256, nullable=false)
     */
    private $texteInfoDemande;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_info_demande", type="date", nullable=false)
     */
    private $dateInfoDemande;

    /**
     * @var bool
     *
     * @ORM\Column(name="lu_non_lu", type="boolean", nullable=false)
     */
    private $luNonLu;

    /**
     * @var \Demande
     *
     * @ORM\ManyToOne(targetEntity="Demande")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_demande", referencedColumnName="id_demande")
     * })
     */
    private $idDemande;



    /**
     * Get idInfo.
     *
     * @return int
     */
    public function getIdInfo()
    {
        return $this->idInfo;
    }

    /**
     * Set texteInfoDemande.
     *
     * @param string $texteInfoDemande
     *
     * @return InfosDemande
     */
    public function setTexteInfoDemande($texteInfoDemande)
    {
        $this->texteInfoDemande = $texteInfoDemande;

        return $this;
    }

    /**
     * Get texteInfoDemande.
     *
     * @return string
     */
    public function getTexteInfoDemande()
    {
        return $this->texteInfoDemande;
    }

    /**
     * Set dateInfoDemande.
     *
     * @param \DateTime $dateInfoDemande
     *
     * @return InfosDemande
     */
    public function setDateInfoDemande($dateInfoDemande)
    {
        $this->dateInfoDemande = $dateInfoDemande;

        return $this;
    }

    /**
     * Get dateInfoDemande.
     *
     * @return \DateTime
     */
    public function getDateInfoDemande()
    {
        return $this->dateInfoDemande;
    }

    /**
     * Set luNonLu.
     *
     * @param bool $luNonLu
     *
     * @return InfosDemande
     */
    public function setLuNonLu($luNonLu)
    {
        $this->luNonLu = $luNonLu;

        return $this;
    }

    /**
     * Get luNonLu.
     *
     * @return bool
     */
    public function getLuNonLu()
    {
        return $this->luNonLu;
    }

    /**
     * Set idDemande.
     *
     * @param \BackOfficeBundle\Entity\Demande|null $idDemande
     *
     * @return InfosDemande
     */
    public function setIdDemande(\BackOfficeBundle\Entity\Demande $idDemande = null)
    {
        $this->idDemande = $idDemande;

        return $this;
    }

    /**
     * Get idDemande.
     *
     * @return \BackOfficeBundle\Entity\Demande|null
     */
    public function getIdDemande()
    {
        return $this->idDemande;
    }
}
