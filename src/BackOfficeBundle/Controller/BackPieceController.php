<?php

namespace BackOfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BackPieceController extends Controller
{
    public function indexAction()
    {
        return $this->render('@BackOffice/BackPiece/BackPiece.html.twig');
    }
}
