<?php

namespace BackOfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BackDemandController extends Controller
{
    public function indexAction()
    {
        return $this->render('@BackOffice/BackDemand/BackDemand.html.twig');
    }
}
